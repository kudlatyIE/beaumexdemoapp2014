package com.example.mystreamplayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.example.networkmonitor.InternetCheck;
import com.example.tabwidgettutorial.MainScreenActivity;
import com.example.tabwidgettutorial.R;


import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class AudioManager extends Activity {
	
	private String [] audioTitle = {"Aural Apocalypse","Cast of Wonders","Guys Games and Beer","Mahabharata Podcast","T3 Podcast","Ouebe Musique",
									"Retroist Podcast","Building Bridges Podcast","The Carter Family","Bascom Lamar Lunsford"};
	private String [] audioDescription = {"22 January 2014","Episode 119","Guys Games and Beer","Episode 100","Episode 299","24 February 2013",
											"Episode 160","Professor Stanley Aronowitz","Wildwood Flower","I wish I was a mole in the ground"};
	private String [] audioUrl = {"https://archive.org/download/AuralApocalypseJanuary22nd2014/AuralApocalypse012214.mp3",
			"https://archive.org/download/Wonders119/Wonders119.mp3",
			"https://archive.org/download/g2bep95aud/g2bep95aud.mp3",
			"https://archive.org/download/Episode100-SummingUp/MBp100.mp3",
			"https://archive.org/download/t3-podcast-299/T3-Podcast-0299.mp3",
			"https://archive.org/download/OuebeMusique24Fevrier2013/Ouebe%20Musique%2024%20fevrier%202013.mp3",
			"https://archive.org/download/RetroistBluesBrothers/Retroist-160-The-Blues-Brothers.mp3",
			"https://archive.org/download/ProfessorStanleyAronowitz-WorkingForAnEconomyByThePeopleForThe_619/aronowitzntl.mp3",
			"https://archive.org/download/Wildwood/WildwoodFlower.mp3",
			"https://archive.org/download/Mole/IWishIWasAMoleInThe.mp3"};
	
	private String [] audioLocal = {"/storage/sdcard0/My Mp3/KatieMelua/01. Shy boy.mp3",
			"/storage/sdcard0/My Mp3/KatieMelua/02. Nine million bicycles.mp3",
			"/storage/sdcard0/My Mp3/KatieMelua/03. Piece by piece.mp3",
			"/storage/sdcard0/My Mp3/KatieMelua/05. Blues in the night.mp3",
			"/storage/sdcard0/My Mp3/KatieMelua/06. Spider's web.mp3",
			"/storage/sdcard0/My Mp3/KatieMelua/07. Blue shoes.mp3",
			"/storage/sdcard0/My Mp3/KatieMelua/08. On the road again.mp3",
			"/storage/sdcard0/My Mp3/KatieMelua/10. Just like heaven.mp3",
			"/storage/sdcard0/My Mp3/KatieMelua/11. I cried for you.mp3",
			"/storage/sdcard0/My Mp3/KatieMelua/12. I do believe in love.mp3"};
	private int [] audioIcon ={R.drawable.ic_music,R.drawable.ic_music,R.drawable.ic_music,R.drawable.ic_music,R.drawable.ic_music,
								R.drawable.ic_music,R.drawable.ic_music,R.drawable.ic_music,R.drawable.ic_music,R.drawable.ic_music};
	
	ListView listView;
	TextView textView;
	Cursor musiccursor;
	private  MediaPlayer mp;
//	private AudioUrlSource audioSource;
	
	private int currentSongIndex = 0;

	private ImageView btnPlay, btnNext, btnPrevious;
//	private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_audio_manager);
		
		if (false == isTablet(getApplicationContext())) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}else{
        //unlock screen orientation (for tablet)
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
		
		btnPlay = (ImageView) findViewById(R.id.btn_play);
		btnNext = (ImageView) findViewById(R.id.btn_next);
		btnPrevious = (ImageView) findViewById(R.id.btn_previous);

		mp = new MediaPlayer();
		Boolean isConnection = false;
		textView = (TextView) findViewById(R.id.song_title);
		TextView tabTitle = (TextView) findViewById(R.id.tab_title);
		listView = (ListView) findViewById(R.id.audio_list);
        AudioListAdapter listAdapter = new AudioListAdapter(AudioManager.this, audioIcon, audioTitle, audioDescription); 
        
        
        // If is internet access display audio list
		isConnection = new InternetCheck(getApplicationContext()).isConnectingToInternet();
        if (false == isConnection) {
        	tabTitle.setText(getString(R.string.msg_no_internet));
        }else {
        	tabTitle.setText(getString(R.string.tab_music));
        	listView.setAdapter(listAdapter); 
        }
		

		listView.setOnItemClickListener(new OnItemClickListener() { 
			@Override
			public void onItemClick(AdapterView<?> adapt, View view, int position, long id) { 
				listView.setLongClickable(false);
				// TODO Auto-generated method stub 
				Boolean isConnection = false;
		        isConnection = new InternetCheck(getApplicationContext()).isConnectingToInternet();
		        if (false == isConnection) {
		        	Toast.makeText(getApplicationContext(), getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
		        }else {
		        	
		        
				currentSongIndex = position;
				if (!mp.isPlaying()){
					mp.start();
					playSong(currentSongIndex);
					btnPlay.setImageResource(R.drawable.ic_pause);
				}else {
					playSong(currentSongIndex);
				}
		        }//-------
			} 
		}); 
			
		// click PLAY button	------------------------------------------------------------------	
		btnPlay.setOnClickListener(new View.OnClickListener() {
			
				
				@Override
				public void onClick(View arg0) {
					Boolean isConnection = false;
			        isConnection = new InternetCheck(getApplicationContext()).isConnectingToInternet();
			        if (false == isConnection) {
			        	Toast.makeText(getApplicationContext(), getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
			        }else {
					
					listView.setLongClickable(false);
					// check for already playing
					if(mp.isPlaying()){
						if(mp!=null){
							mp.pause();
							// Changing button image to play button
							btnPlay.setImageResource(R.drawable.ic_play);
						}
					}else{
						// Resume song
						if(mp!=null){
							mp.start();
							if(currentSongIndex == 0){
								playSong(0);
							}
							// Changing button image to pause button
							btnPlay.setImageResource(R.drawable.ic_pause);
						}
					}
			        }//--------
				}
			});
	// click NEXT button ---------------------------------------------------------------------------
		btnNext.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Boolean isConnection = false;
		        isConnection = new InternetCheck(getApplicationContext()).isConnectingToInternet();
		        if (false == isConnection) {
		        	Toast.makeText(getApplicationContext(), getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
		        }else {
				listView.setLongClickable(false);
				// check if next song is there or not
				if(currentSongIndex < (audioTitle.length - 1)){
					currentSongIndex = currentSongIndex + 1;
					playSong(currentSongIndex);
					
				}else{
					// play first song
					currentSongIndex = 0;
					playSong(currentSongIndex);
					
				}
		        }//---------
			}
		});
	// click PREVIOSU button ---------------------------------------------------------------------------
		btnPrevious.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Boolean isConnection = false;
		        isConnection = new InternetCheck(getApplicationContext()).isConnectingToInternet();
		        if (false == isConnection) {
		        	Toast.makeText(getApplicationContext(), getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
		        }else {
				listView.setLongClickable(false);
				if(currentSongIndex > 0){
					currentSongIndex = currentSongIndex - 1;
					playSong(currentSongIndex);
				}else{
					// play last song
					currentSongIndex = audioTitle.length-1;
					playSong(currentSongIndex);
					
				}
			}//----
			}
		});
		
	
	}
// end onCreate()-----------------------------------------------------
	

// play song from url when call, and set the title in audio_console -----------------------------------------------------	
	public void  playSong(int songIndex){
		System.gc();
		//lock current screen orientation when start buffering
		lockOrientation(this);
		// Play song
		try {
			if (mp.isPlaying()) {
				mp.stop();
                mp.reset();
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} 

		new Player().execute(audioLocal);

        	// Changing Button Image to pause image
			btnPlay.setImageResource(R.drawable.ic_pause);			
	}

//---------------------------------------------------------------
	@Override
	public void onResume(){
		super.onResume();
		if (false == isTablet(getApplicationContext())) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
	}
	
	@Override
	public void onPause(){ //reset song index, song title in console and btnPlay?Pause position when switch to another tab
		super.onPause();
		if(mp !=null){
			try {
				currentSongIndex=0;
				btnPlay.setImageResource(R.drawable.ic_play);
				textView.setText("");
				mp.stop();
			}catch (NullPointerException npe) {
				npe.printStackTrace();
			}catch (Exception ex) {
				ex.printStackTrace();
			}
		
		}
	}
	@Override
	public void onStop(){
		super.onStop();
		if(mp!=null) {
			mp.stop();
		}
	}

	
	@Override
	 public void onDestroy(){
		super.onDestroy();
		try {
			mp.stop();
			mp.release();
			mp=null;
		}catch (NullPointerException npe) {
			npe.printStackTrace();
		}catch (RuntimeException re) {
			re.printStackTrace();
		}catch (Exception ex) {
			ex.printStackTrace();
		}finally {
			System.gc();
			
			
		}
	 }
//--------------------------------------------------------------
// class for buffering audio file from url (local or remote)
	class Player extends AsyncTask<String, Void, Boolean> {
	    private ProgressDialog progress;

	    @Override // stuff to do in background thread
	    protected Boolean doInBackground(String... params) {
	        Boolean prepared;
//	        if (this.isCancelled()){
//	        	return null;	
//	        }
	        try {

	            mp.setDataSource(params[currentSongIndex]);

	            mp.setOnCompletionListener(new OnCompletionListener() {

	                @Override
	                public void onCompletion(MediaPlayer mp) {
	                    mp.stop();
	                    mp.reset();
	                }
	            });
	            mp.prepare();
	            prepared = true;
	            
	        } catch (IllegalArgumentException e) {
	            Log.d("IllegarArgument", e.getMessage());
	            prepared = false;
	            e.printStackTrace();
	        } catch (SecurityException e) {
	            prepared = false;
	            e.printStackTrace();
	        } catch (IllegalStateException e) {
	            prepared = false;
	            e.printStackTrace();
	        } catch (IOException e) {
	            prepared = false;
	            e.printStackTrace();
	        }
	        return prepared;
	        
	    }
    
	    @Override
	    protected void onPostExecute(Boolean result) {
	       
	        super.onPostExecute(result);
	        if (progress.isShowing()) {
	        	progress.cancel();
	        }
	        Log.d("Prepared", "//" + result);
	        try {
	        	mp.start();
	        }catch (NullPointerException npe){
	        	npe.printStackTrace();
	        }catch (IllegalArgumentException iae) {
	        	iae.printStackTrace();
	        }
	    }

	    public Player() {
	        progress = new ProgressDialog(AudioManager.this); 
	    }
	    
	    //before playing there will be a need for
	    //buffering to occur

	    @Override
	    protected void onPreExecute() {
	        
	        super.onPreExecute();
	        this.progress.setMessage("Buffering...");
	        this.progress.show();
	        textView.setText(audioTitle[currentSongIndex]);

	    }
	}
    @Override
    public void onBackPressed() {

    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AudioManager.this);

    alertDialog.setPositiveButton("Yes", new OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            finish();
        }
    });

    alertDialog.setNegativeButton("No", null);

    alertDialog.setMessage("Do you want to exit?");
    alertDialog.setTitle("Buzz Entertainment");
    alertDialog.show();
    }
    
    // lock screen orientation (on buffering time: async task)
    public  void lockOrientation(Activity activity) {
        Display display = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        int tempOrientation = activity.getResources().getConfiguration().orientation;
        int orientation = 0;
        switch(tempOrientation)
        {
        case Configuration.ORIENTATION_LANDSCAPE:
            if(rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90)
                orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
            else
                orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
            break;
        case Configuration.ORIENTATION_PORTRAIT:
            if(rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_270)
                orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            else
                orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
        }
        activity.setRequestedOrientation(orientation);
    }
    // check is devide a tablet (if phone then screen rotation will be locked in portrait)
    public boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }
}
