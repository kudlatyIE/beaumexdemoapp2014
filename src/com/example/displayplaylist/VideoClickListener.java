package com.example.displayplaylist;

import com.example.getytplaylist.*;

public interface VideoClickListener {

	public void onVideoClicked(Video video);
	
}
