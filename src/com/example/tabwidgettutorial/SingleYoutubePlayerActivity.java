package com.example.tabwidgettutorial;

import android.os.Bundle;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubePlayer.Provider;

import android.widget.Toast;
import com.example.userytplaylist.*;

public class SingleYoutubePlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener  {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_youtube_player);
		YouTubePlayerView ytPlayerView = (YouTubePlayerView) findViewById(R.id.youtubeplayerview);
		ytPlayerView.initialize(DeveloperKeys.YT_API_KEY, this);
	}
	
	@Override
	public void onInitializationFailure(Provider provider, YouTubeInitializationResult result) {
		Toast.makeText(getApplicationContext(), "onInitializationFailure()....", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {
		if (!wasRestored) {
			player.cueVideo(getIntent().getExtras().getString("VideoID"));
		}
	}

}
