package com.example.tabwidgettutorial;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.getytplaylist.*;

import com.example.networkmonitor.InternetCheck;
import com.example.tabwidgettutorial.R;
import com.example.userytplaylist.DeveloperKeys;
import com.example.userytplaylist.PlayVideoActivity;

import com.example.displayplaylist.*;

public class EBuzzActivity extends Activity implements VideoClickListener {
    // A reference to our list that will hold the video details
	private VideosListView listView;
	int listCounter;
	boolean isList;
   	Boolean isConnection = false;

    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (false == isTablet(getApplicationContext())) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}else{
        //unlock screen orientation (for tablet)
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
        
        setContentView(R.layout.activity_display_playlist);
        TextView tabTitle = (TextView) findViewById(R.id.tab_title);
        isConnection = new InternetCheck(getApplicationContext()).isConnectingToInternet();
        if (false == isConnection) {
        	tabTitle.setText(getString(R.string.msg_no_internet));
        }else {
        	tabTitle.setText(getString(R.string.tab_buzz));
        }
        
        new Thread(new GetVideosTask(responseHandler, DeveloperKeys.EBUUZ_URL)).start();
        
        listView = (VideosListView) findViewById(R.id.videosListView);
        // Here we are adding this activity as a listener for when any row in the List is 'clicked'
        // The activity will be sent back the video that has been pressed to do whatever it wants with
        // in this case we will retrieve the URL of the video and fire off an intent to view it
        listView.setOnVideoClickListener(this);
        
        // prepare to set png.9 as BUZZ backgroung
        Resources res = getResources();
        Drawable drawable = null;
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.video_listLayout);
          
    }
   
	Handler responseHandler = new Handler() {
		public void handleMessage(Message msg) {
			populateListWithVideos(msg);
		};
	};

	private void populateListWithVideos(Message msg) {
		Library lib = (Library) msg.getData().get(GetVideosTask.LIBRARY);
		listView.setVideos(lib.getVideos());
	}
	
	@Override
	protected void onStop() {
		responseHandler = null;
		super.onStop();
	}

	// This is the interface method that is called when a video in the listview is clicked!
	// The interface is a contract between this activity and the listview
	@Override
	public void onVideoClicked(Video video) {

		Intent intent = new Intent(this, PlayVideoActivity.class);

		intent.putExtra("VideoID",video.getVideoId());
		intent.putExtra("VideoDescription",video.getDescription() );
		intent.putExtra("VideoUrl", video.getVideoUrl());
		isConnection = new InternetCheck(getApplicationContext()).isConnectingToInternet();
        if (false == isConnection) {
        	Toast.makeText(getApplicationContext(), getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
        }else {
        	startActivity(intent);
        }
	}
    @Override
    public void onBackPressed() {

    AlertDialog.Builder alertDialog = new AlertDialog.Builder(EBuzzActivity.this);

    alertDialog.setPositiveButton("Yes", new OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            finish();
        }
    });

    alertDialog.setNegativeButton("No", null);

    alertDialog.setMessage("Do you want to exit?");
    alertDialog.setTitle("Buzz Entertainment");
    alertDialog.show();
    }
    public boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }
}
