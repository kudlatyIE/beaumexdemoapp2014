package com.example.tabwidgettutorial;

import android.os.Bundle;
import com.example.mystreamplayer.*;
import com.example.networkmonitor.InternetCheck;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

@SuppressWarnings("deprecation")
public class MainScreenActivity extends TabActivity {
	
   	Boolean isConnection = false;
   	InternetCheck checker;
   	TabHost tabHost;
   	int myTab;
   	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
                    
        setContentView(R.layout.activity_main_screen);
        if (false == isTablet(getApplicationContext())) {
        	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
        	//unlock screen orientation (for tablet)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        	}
        Resources appResources = getResources();
        tabHost = getTabHost();
            		
        tabHost.setOnTabChangedListener(new OnTabChangeListener() {
        	@Override
            public void onTabChanged(String arg0) {         
        		Log.i("***Selected Tab", "Im currently in tab with index::" + tabHost.getCurrentTab());
        		myTab = tabHost.getCurrentTab();
             }       
         });
            		
         // Ebuzz Tab
         Intent intentEbuzz = new Intent().setClass(this, EBuzzActivity.class);
         intentEbuzz.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
         TabSpec tabSpecEbuzz = tabHost.newTabSpec("").setIndicator("", appResources.getDrawable(R.drawable.icon_config_buzz)).setContent(intentEbuzz);
            		
         // Music Tab
         Intent intentMusic = new Intent().setClass(this, AudioManager.class);
         intentMusic.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
         TabSpec tabSpecMusic = tabHost.newTabSpec("").setIndicator("", appResources.getDrawable(R.drawable.icon_config_music)).setContent(intentMusic);
            		
          // Cinema Tab
          Intent intentCinema = new Intent().setClass(this, CinemaActivity.class);
          intentCinema.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
          TabSpec tabSpecCinema = tabHost.newTabSpec("").setIndicator("", appResources.getDrawable(R.drawable.icon_config_cinema)).setContent(intentCinema);
            		
           // DVD Tab
           Intent intentDVD = new Intent().setClass(this, DVDActivity.class);
           intentDVD.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           TabSpec tabSpecDVD = tabHost.newTabSpec("").setIndicator("", appResources.getDrawable(R.drawable.icon_config_dvd)).setContent(intentDVD);		
            		
           // Festival Tab
           Intent intentFestival = new Intent().setClass(this, FestivalActivity.class);
           intentFestival.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           TabSpec tabSpecFestival = tabHost.newTabSpec("").setIndicator("", appResources.getDrawable(R.drawable.icon_config_festival)).setContent(intentFestival);
            		
            		         			
           tabHost.addTab(tabSpecEbuzz);
           tabHost.addTab(tabSpecMusic);
           tabHost.addTab(tabSpecCinema);
           tabHost.addTab(tabSpecDVD);
           tabHost.addTab(tabSpecFestival); 	
      }

            
      public boolean isTablet(Context context) {
    	  boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
          boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
          return (xlarge || large);
      }
            

      @Override
      protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	  super.onRestoreInstanceState(savedInstanceState);
          if( savedInstanceState != null ){
//        Toast.makeText(getApplicationContext(), "saved tab = "+savedInstanceState.getInt("tabState"), Toast.LENGTH_SHORT).show();
        	  tabHost.setCurrentTab(savedInstanceState.getInt("tabState"));
          }else {
            	tabHost.setCurrentTab(0);
                }
          }
          
      @Override
      protected void onSaveInstanceState(Bundle outState) {          
    	  outState.putInt("tabState", myTab);
          super.onSaveInstanceState(outState);
      }
            
}