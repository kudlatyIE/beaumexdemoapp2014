package com.example.tabwidgettutorial;

import com.example.networkmonitor.*;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreen extends Activity {

	// Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    public final static String EXTRA_MESSAGE = "com.example.test3.MESSAGE";
    TextView tvstatus;
//    private boolean checker = false;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        
        if (false == isTablet(getApplicationContext())) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}else{
        //unlock screen orientation (for tablet)
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
        
    	
   	 // tu sprawdzimy czy jest dostepny internecik: do() while ()....
   	Boolean isConnection = false;
   	InternetCheck checker;
   	TextView tv;
   	
   	checker = new InternetCheck(getApplicationContext());
   	isConnection = checker.isConnectingToInternet();  
	
	ImageView img;
	if (false == isConnection) {
			img = (ImageView) findViewById(R.id.splash_welcome);
			img.setBackgroundResource(R.drawable.ic_no_internet_access);
			
	} else {
		img = (ImageView) findViewById(R.id.splash_welcome);
		img.setBackgroundResource(R.drawable.ic_welcome_to);

//------------ przejscie do main screen

        new Handler().postDelayed( new Runnable() {
 
            @Override
            public void run() {
                Intent i = new Intent(SplashScreen.this, MainScreenActivity.class);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT );
 
    }
	
   }
    public boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }
}

