package com.example.getytplaylist;

import java.io.Serializable;
import java.util.List;

/**
 * This is the 'library' of all the users videos
 * 
 * @author paul.blundell
 */
public class Library implements Serializable{
	// The username of the owner of the library
	private String youtubeUrl;
	// A list of videos that the user owns
	private List<Video> videos;
	
	public Library(String youtubeUrl, List<Video> videos) {
		this.youtubeUrl = youtubeUrl;
		this.videos = videos;
	}

	/**
	 * @return the user name
	 */
	public String getYoutubeUrl() {
		return youtubeUrl;
	}

	/**
	 * @return the videos
	 */
	public List<Video> getVideos() {
		return videos;
	}
}
