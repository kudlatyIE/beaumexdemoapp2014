package com.example.getytplaylist;

import java.io.Serializable;

/**
 * This is a representation of a users video off YouTube
 * @author paul.blundell
 * 
 * customised by kudlatyIE
 */
public class Video implements Serializable {
	// The title of the video
	private String title;
	// A link to the video on youtube
	private String videoId;
	// A link to a still image of the youtube video
	private String thumbUrl;
	private String description;
	private String videoUrl;
	
	public Video(String title, String videoId, String description, String thumbUrl, String videoUrl) {
		super();
		this.title = title;
		this.videoId = videoId;
		this.description = description;
		this.thumbUrl = thumbUrl;
		this.videoUrl = videoUrl;
	}

	/**
	 * @return the title of the video
	 */
	public String getTitle(){
		return title;
	}

	/**
	 * @return the ID to this video on youtube
	 */
	public String getVideoId() {
		return videoId;
	}
	
	/**
	 * return the video's description
	 */
	public String getDescription(){
		return description;
	}

	/**
	 * @return the thumbUrl of a still image representation of this video
	 */
	public String getThumbUrl() {
		return thumbUrl;
	}
	public String getVideoUrl() {
		return videoUrl;
	}
}
