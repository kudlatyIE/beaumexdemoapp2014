package com.example.userytplaylist;

import android.os.Bundle;

import com.example.tabwidgettutorial.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubePlayer.Provider;

import android.widget.TextView;
import android.widget.Toast;

import com.example.tabwidgettutorial.R;


public class PlayVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener  {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play_video);
		YouTubePlayerView ytPlayerView = (YouTubePlayerView) findViewById(R.id.youtubeplayerview);
		ytPlayerView.initialize(DeveloperKeys.YT_API_KEY, this);
		TextView videoDescription, videoUrl;
//		videoUrl = (TextView) findViewById(R.id.video_url);
		videoDescription = (TextView) findViewById(R.id.video_description);
		
		
		try {
//			videoUrl.setText(getIntent().getExtras().getString("VideoUrl"));
			videoDescription.setText(getIntent().getExtras().getString("VideoDescription"));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		
		
	}
	
	@Override
	public void onInitializationFailure(Provider provider, YouTubeInitializationResult result) {
		Toast.makeText(getApplicationContext(), "onInitializationFailure()....", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {
		if (!wasRestored) {
			
			player.cueVideo(getIntent().getExtras().getString("VideoID"));
		}
	}


}
