/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.tabwidgettutorial;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int gray=0x7f040000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int border=0x7f020000;
        public static final int border_audio=0x7f020001;
        public static final int gradient_bg=0x7f020002;
        public static final int gradient_bg_hover=0x7f020003;
        public static final int ic9_big_squeezed_350px=0x7f020004;
        public static final int ic9_new_splash=0x7f020005;
        public static final int ic9_small_squeezed_350=0x7f020006;
        public static final int ic_audio_80px=0x7f020007;
        public static final int ic_big_squeezed_350px=0x7f020008;
        public static final int ic_buzz=0x7f020009;
        public static final int ic_buzz_slim=0x7f02000a;
        public static final int ic_cinema=0x7f02000b;
        public static final int ic_dvd=0x7f02000c;
        public static final int ic_episodes=0x7f02000d;
        public static final int ic_festival=0x7f02000e;
        public static final int ic_lanch_buzz=0x7f02000f;
        public static final int ic_launcher=0x7f020010;
        public static final int ic_music=0x7f020011;
        public static final int ic_music_slim=0x7f020012;
        public static final int ic_next=0x7f020013;
        public static final int ic_no_internet_access=0x7f020014;
        public static final int ic_pause=0x7f020015;
        public static final int ic_play=0x7f020016;
        public static final int ic_powered_by=0x7f020017;
        public static final int ic_previous=0x7f020018;
        public static final int ic_welcome_to=0x7f020019;
        public static final int icon_config_buzz=0x7f02001a;
        public static final int icon_config_cinema=0x7f02001b;
        public static final int icon_config_dvd=0x7f02001c;
        public static final int icon_config_festival=0x7f02001d;
        public static final int icon_config_music=0x7f02001e;
        public static final int list_selector=0x7f02001f;
        public static final int next_80px=0x7f020020;
        public static final int previous_80px=0x7f020021;
        public static final int tab_separator=0x7f020022;
    }
    public static final class id {
        public static final int LinearLayout01=0x7f090009;
        public static final int action_settings=0x7f090017;
        public static final int audio_consola=0x7f090002;
        public static final int audio_list=0x7f090001;
        public static final int btn_next=0x7f090006;
        public static final int btn_play=0x7f090005;
        public static final int btn_previous=0x7f090004;
        public static final int icon=0x7f09000e;
        public static final int imgLogo=0x7f09000d;
        public static final int internet_check=0x7f09000c;
        public static final int item1=0x7f09000f;
        public static final int item2=0x7f090010;
        public static final int powered=0x7f090016;
        public static final int song_title=0x7f090003;
        public static final int splash_logo=0x7f090015;
        public static final int splash_welcome=0x7f090014;
        public static final int tab_title=0x7f090000;
        public static final int userVideoThumbImageView=0x7f090011;
        public static final int userVideoTitleTextView=0x7f090012;
        public static final int video_description=0x7f09000b;
        public static final int video_description_text=0x7f090013;
        public static final int video_listLayout=0x7f090007;
        public static final int videosListView=0x7f090008;
        public static final int youtubeplayerview=0x7f09000a;
    }
    public static final class layout {
        public static final int activity_audio_manager=0x7f030000;
        public static final int activity_display_playlist=0x7f030001;
        public static final int activity_main_screen=0x7f030002;
        public static final int activity_play_video=0x7f030003;
        public static final int activity_single_youtube_player=0x7f030004;
        public static final int activity_splash_screen=0x7f030005;
        public static final int grid_list_layout=0x7f030006;
        public static final int list_item_user_video=0x7f030007;
        public static final int splash_screen=0x7f030008;
    }
    public static final class menu {
        public static final int display_playlist=0x7f080000;
        public static final int ebuzz=0x7f080001;
        public static final int main=0x7f080002;
        public static final int main_screen=0x7f080003;
        public static final int play_video=0x7f080004;
        public static final int share_video=0x7f080005;
        public static final int single_youtube_player=0x7f080006;
        public static final int splash_screen=0x7f080007;
        public static final int splash_screen_spin=0x7f080008;
        public static final int tab1=0x7f080009;
        public static final int tab2=0x7f08000a;
        public static final int tab3=0x7f08000b;
        public static final int tab4=0x7f08000c;
        public static final int tab5=0x7f08000d;
    }
    public static final class string {
        public static final int action_settings=0x7f060001;
        public static final int app_name=0x7f060000;
        public static final int hello_world=0x7f060002;
        public static final int msg_no_internet=0x7f060016;
        public static final int tab_buzz=0x7f06000f;
        public static final int tab_cinema=0x7f060010;
        public static final int tab_dvd=0x7f060011;
        public static final int tab_festival=0x7f060012;
        public static final int tab_music=0x7f060013;
        public static final int title_activity_audio_manager=0x7f06000d;
        public static final int title_activity_display_playlist=0x7f060009;
        public static final int title_activity_ebuzz=0x7f060008;
        public static final int title_activity_main_screen=0x7f06000e;
        public static final int title_activity_play_video=0x7f06000a;
        public static final int title_activity_share_video=0x7f06000b;
        public static final int title_activity_single_youtube_player=0x7f06000c;
        public static final int title_activity_splash_screen=0x7f060015;
        public static final int title_activity_splash_screen_spin=0x7f060014;
        public static final int title_activity_tab1=0x7f060003;
        public static final int title_activity_tab2=0x7f060004;
        public static final int title_activity_tab3=0x7f060005;
        public static final int title_activity_tab4=0x7f060006;
        public static final int title_activity_tab5=0x7f060007;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
    }
}
