v.09 2014/04/03

Added:
- Cinema: YT playlist (JSON),
- Music: simple audio player (streaming file),
- EBuzz: list with videos, will be pleyed in single video player.
(now time to sleep)

v.1.01 2014/04/08

Added:
- Music: listView with remote free mp3 sample and simple player,
- DVD: videos from http://www.youtube.com/dvdreleasedatenotify,
- Festival: YT search (festival reviews on ireland am),
- Buzz: playlist with Etntertainment Buzz episodes (PLk3PsgQ4dN6ntTobt7zORIzZCH2y-Z5ph) from kudlatyIE.

v1.02 2014-04-10

Added:
- SplashScreen with Internet Access checker (old one),
- new customized icons for tabs, and audio player,
- improved listViews for videos and audio, fixed some other minor stuff in layouts.

v1.03 2014-04-13

Added:
- New splash screen (no any more stupid animations),
- new icons, img (9.png),
- fixed bug in audio player.

v1.04 2014-04-14

Added:
- Handle screen rotation: different for phone and tablet,
- handle back button,
....?

v1.05 2014-04-14

Added:
- Handle remain current tab after screen rotation,
- fixed bug with setting portrait orientation in AudioManager (for phone mode)
